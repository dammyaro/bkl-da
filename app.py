from flask import Flask, request, render_template, send_file
import pandas as pd
import requests
import csv
import time
import sys
import os
from postmarker.core import PostmarkClient

current_dir = os.path.dirname(os.path.realpath(__file__))
mozscape_path = os.path.join(current_dir, 'mozscape')
sys.path.append(mozscape_path)

from mozscape import Mozscape

app = Flask(__name__)
client = Mozscape('mozscape-3301ba3455', '95b25eb3482276fd40c80eee8b7d135')
postmark = PostmarkClient(server_token='85a16a23-0a55-4484-9cd5-7255efef126a')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/process_csv', methods=['POST'])
def process_csv():
    if 'csv_file' not in request.files:
        return "No file part"

    email = request.form.get('email')
    file = request.files['csv_file']
    if file.filename == '':
        return "No selected file"

    if file:
        data = file.read().decode('utf-8').splitlines()
        reader = csv.DictReader(data)
        domain_authorities = []

        for row in reader:
            # print(row)
            name = row.get('\ufeffname')
            if name:
                # domain = name.split('//')[1].split('/')[0] #ca
                # domain_authority = get_domain_authority(permalink)  # Moz API call
                print(name)
                domain_authority = client.urlMetrics(name)['pda']
                domain_authorities.append({'name': name, 'domain_authority': domain_authority})
                time.sleep(11)  # 11-second delay between requests

        if domain_authorities:
            df = pd.DataFrame(domain_authorities)
            output_csv = 'domain_authorities.csv'
            df.to_csv(output_csv, index=False)

            # Send the email with the file attachment
            try:
                postmark.emails.send(
                    From='support@fyndr.tech',
                    To=email,
                    Subject='Domain Authority Report',
                    HtmlBody='<p>Attached is the Domain Authority report for your domains.</p>',
                    Attachments=[output_csv]  # Attach the CSV file
                )
            except Exception as e:
                print("Error sending email", str(e))
            return send_file(output_csv, as_attachment=True)
        else:
            return "No valid domains found in the CSV file."

def get_domain_authority(domain):
    # Implement Moz API call here
    # Replace 'your_access_id' and 'your_secret_key' with your actual Moz API credentials
    print(domain)
    base_url = 'https://lsapi.seomoz.com/v2/url_metrics'
    access_id = 'mozscape-3301ba3455'
    secret_key = '95b25eb3482276fd40c80eee8b7d135'

    headers = {
        'Authorization': f'Basic {access_id}:{secret_key}'
    }

    # payload = {
    #     'AccessID': access_id,
    #     'Expires': int(time.time()) + 300,
    #     'Cols': '103079215108',
    #     'Offset': '0',
    #     'Limit': '1',
    #     'SourceCols': '4',
    #     'TargetCols': '4',
    #     'Filters': 'external+4xx+2xx+3xx'
    # }

    # signature = (access_id + '\n' + str(payload['Expires'])).encode('utf-8') + secret_key.encode('utf-8')
    # payload['Signature'] = signature

    # response = requests.get(f'{base_url}?URL={domain}', params=payload)
    params = {
        'target': domain,
    }
    response = requests.get(base_url, headers=headers, params=params)

    if response.status_code == 200:
        data = response.json()
        print(data)
        domain_authority = data['pda']
        return domain_authority
    else:
        return None

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
